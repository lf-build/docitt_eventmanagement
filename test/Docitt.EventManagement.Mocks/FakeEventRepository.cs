﻿using Docitt.EventManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace docitt.EventManagement.Mocks
{
    public class FakeEventRepository :IEventRepository
    {
        public List<IEventModel> EventInMemoryData { get; set; } = new List<IEventModel>();
        public FakeEventRepository()
        {
        }

        public void Add(IEventModel item)
        {
            item.Id = Guid.NewGuid().ToString("N");
            EventInMemoryData.Add(item);
        }

        public Task<IEnumerable<IEventModel>> All(Expression<Func<IEventModel, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            return Task.Run(() =>
            {
                return EventInMemoryData
                    .AsQueryable()
                    .Where(query)
                    .AsEnumerable();
            });
        }

        public int Count(Expression<Func<IEventModel, bool>> query)
        {
            return EventInMemoryData
                .AsQueryable()
                .Count(query);
        }

        public Task<IEnumerable<IEventModel>> GetAll(string entityType, string entityId)
        {
            return Task.FromResult<IEnumerable<IEventModel>>(EventInMemoryData.AsEnumerable());
        }

        public Task<IEnumerable<IEventModel>> GetByMonth(string entityType, string entityId, DateTimeOffset firstDayofMonth, DateTimeOffset lastDayofMonth)
        {
           return Task.FromResult<IEnumerable<IEventModel>>(EventInMemoryData.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.EventDate >= firstDayofMonth && p.EventDate <= lastDayofMonth).ToList());
        }

        public Task<IEnumerable<IEventModel>> GetByDate(string entityType, string entityId, DateTimeOffset date)
        {
            return Task.FromResult<IEnumerable<IEventModel>>(EventInMemoryData.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.EventDate >= date).ToList());
        }

        public void Remove(IEventModel item)
        {
            throw new NotImplementedException();
        }

        public void Update(IEventModel item)
        {
            throw new NotImplementedException();
        }

        public Task<IEventModel> Get(string id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IEventModel>> GetAll(string entityType)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IEventModel>> GetByDate(string entityType, DateTimeOffset date)
        {
            throw new NotImplementedException();
        }

        public Task<IEventModel> GetById(string entityType, string entityId, string eventId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IEventModel>> GetByIds(string entityType, string entityId, IEnumerable<string> eventIds)
        {
            throw new NotImplementedException();
        }
    }
}
