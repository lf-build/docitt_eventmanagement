﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;
using Docitt.EventManagement.Api.Controllers;

namespace Docitt.EventManagement.Api.Tests
{
    public class ApiControllerTests
    {
        private Mock<Services.IEventService> EventManagementService { get; }

        private ApiController Api { get; }

        public ApiControllerTests()
        {
            EventManagementService = new Mock<Services.IEventService>();     

            Api = new ApiController(EventManagementService.Object);
        }

        [Fact]
        public void Controller_With_Null_Services_Should_Throw_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => new ApiController(null));
        }

        //[Fact]
        //public void Controller_ADD_With_Null_EntityType_EntityId_Should_Thorw_ArgumentException()
        //{
        //    EventManagementService.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<string>(),It.IsAny<RequestPayload>())).Throws(new ArgumentException("entityType is mandatory"));

        //    var result = (ErrorResult) Api.Add("", "12345",new RequestPayload());
        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 400);
        //    EventManagementService.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<RequestPayload>())).Throws(new ArgumentException("entityid is mandatory"));

        //    result = (ErrorResult)Api.Add("application", "", new RequestPayload());
        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 400);            
        //}

        //[Fact]
        //public void Controller_ADD_With_Null_Payload_Should_Thorw_ArgumentException()
        //{
        //    RequestPayload payload = null;
        //    EventManagementService.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<RequestPayload>())).Throws(new ArgumentException("request is mandatory"));
        //    var result = (ErrorResult)Api.Add("application", "12345", payload);
        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 400);
        //}


        //[Fact]
        //public void Controller_ADD_Returns_Result_OnSuccess()
        //{
            
        //    var payload = new RequestPayload
        //    {  
        //        EventDate = DateTime.Now,
        //        Description = "Payment due"
        //    };

        //    var result = (HttpStatusCodeResult) Api.Add("application","12345", payload);

        //    Assert.NotNull(result);
        //    Assert.Equal(204, result.StatusCode);
        //}


        [Fact]
        public async void Controller_GetAll_With_Null_EnityId_EntityType_Throw_ArgumentException()
        {
            EventManagementService.Setup(x => x.GetAll(It.IsAny<string>(), It.IsAny<string>())).Throws(new ArgumentException("entitytype is mandatory"));
            var result = (ErrorResult)await Api.GetAll("", "12345");

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);


            EventManagementService.Setup(x => x.GetAll(It.IsAny<string>(), It.IsAny<string>())).Throws(new ArgumentException("entityid is mandatory"));
            result = (ErrorResult)await Api.GetAll("application", "");

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }


        [Fact]
        public async void Controller_GetAllEvents_Returns_ErrorResult_OnFailure()
        {
            EventManagementService.Setup(x => x.GetAll(It.IsAny<string>(), It.IsAny<string>()))
                .ThrowsAsync(new InvalidOperationException("Unknown exception"));

            var result = (ErrorResult)await Api.GetAll("application", "000001");

            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void Controller_GetAllEvents_Returns_Result_OnSuccess()
        {
            EventManagementService.Setup(x => x.GetAll(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new List<EventModel>());

            var result = (HttpOkObjectResult)await Api.GetAll("application", "jondoe");

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<List<EventModel>>(result.Value);
        }

        [Fact]
        public async void Controller_GetByMonth_Returns_Result_OnSuccess()
        {
            EventManagementService.Setup(x => x.GetByMonth(It.IsAny<string>(), It.IsAny<string>(),It.IsAny<int>(),It.IsAny<int>()))
                .ReturnsAsync(new List<EventModel>());

            var result = (HttpOkObjectResult)await Api.GetByMonth("application", "jondoe",4,2017);

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<List<EventModel>>(result.Value);
        }

        [Fact]
        public async void Controller_GetByDate_Returns_Result_OnSuccess()
        {
            EventManagementService.Setup(x => x.GetByDate(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTimeOffset>()))

                .ReturnsAsync(new List<EventModel>());
           
            var result = (HttpOkObjectResult)await Api.GetByDate("application", "12345", DateTime.UtcNow.ToString());

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<List<EventModel>>(result.Value);
        }


        [Fact]
        public async void Controller_RemoveEvent_With_Null_EntityType_EntityId_EventId_Should_Thorw_ArgumentException()
        {
            EventManagementService.Setup(x => x.RemoveEvent(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws(new ArgumentException("entityType is mandatory"));

            var result = (ErrorResult) await Api.RemoveEvent("", "12345","12345");
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
            EventManagementService.Setup(x => x.RemoveEvent(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws(new ArgumentException("entityid is mandatory"));

            result = (ErrorResult)await Api.RemoveEvent("", "12345", "12345");
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            EventManagementService.Setup(x => x.RemoveEvent(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws(new ArgumentException("eventid is mandatory"));

            result = (ErrorResult)await Api.RemoveEvent("", "12345", "");
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }

        

        [Fact]
        public async void Controller_RemoveEvent_With_Success()
        {
            EventManagementService.Setup(x => x.RemoveEvent(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(true);
            var result = (HttpOkObjectResult)await Api.RemoveEvent("application", "12345","123");
            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);         
        }

        public async void Controller_UpSertEvent_With_Null_EntityType_EntityId_Should_Thorw_ArgumentException()
        {
            EventManagementService.Setup(x => x.RemoveEvent(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws(new ArgumentException("entityType is mandatory"));

            var result = (ErrorResult)await Api.RemoveEvent("", "12345", "12345");
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
            EventManagementService.Setup(x => x.RemoveEvent(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws(new ArgumentException("entityid is mandatory"));

            result = (ErrorResult)await Api.RemoveEvent("", "12345", "12345");
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);            
        }

        [Fact]
        public async void Controller_UpSertEvent_With_Null_Payload_Should_Thorw_ArgumentException()
        {
            List<RequestPayload> payload = null;
            EventManagementService.Setup(x => x.UpSertEvent(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<RequestPayload>>())).Throws(new ArgumentException("payload is mandatory"));
            var result = (ErrorResult)await Api.UpSertEvent("application", "12345", payload);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async void Controller_UpSertEvent_With_Success()
        {
            List<RequestPayload> payload =new List<RequestPayload>();
            EventManagementService.Setup(x => x.UpSertEvent(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<RequestPayload>>())).ReturnsAsync(true);
            RequestPayload objRequest = new RequestPayload() {
                EventDate = DateTime.Now,
                Description = "test event",
                isAdded = false
            };
            payload.Add(objRequest);
            RequestPayload objRequestAdd = new RequestPayload()
            {
                EventDate = DateTime.Now,
                Description = "test event",
                isAdded = true
            };

            payload.Add(objRequestAdd);
            var result = (HttpOkObjectResult)await Api.UpSertEvent("application", "12345", payload);
            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
        }

    }
}
