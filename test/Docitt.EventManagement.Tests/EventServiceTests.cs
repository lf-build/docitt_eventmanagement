﻿using docitt.EventManagement.Mocks;
using Docitt.EventManagement.Configuration;
using Docitt.EventManagement.Services;
using Docitt.ReminderService.Services;
using LendFoundry.Email;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup;
using LendFoundry.TemplateManager;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Docitt.EventManagement.Tests
{
    public class EventServiceTests
    {
        private FakeEventRepository EventRepository { get; }
      
        private Mock<ITenantTime> TenantTime { get; }
        private Mock<IEventHubClient> EventHub { get; }

        private Mock<ILookupService> mockLookUp { get; }
      
        private IEventService Service { get; }

        private Mock<IEmailService> emailService { get; }

        private Mock<ITemplateManagerService> tempalteService { get; }
        private Mock<IReminderService> reminderService { get; }
        private Mock<IEventManagementConfiguration> eventConfiguration;
        public EventServiceTests()
        {
            EventRepository = new FakeEventRepository();

            TenantTime = new Mock<ITenantTime>();
            EventHub = new Mock<IEventHubClient>();
            mockLookUp = new Mock<ILookupService>();
            emailService = new Mock<IEmailService>();
            tempalteService = new Mock<ITemplateManagerService>();
            eventConfiguration = new Mock<IEventManagementConfiguration>();
            reminderService = new Mock<IReminderService>();
            //Validator = new Mock<IValidatorService>();

            Service = new EventService(EventRepository,
                TenantTime.Object
                , EventHub.Object,
                mockLookUp.Object, emailService.Object, tempalteService.Object, eventConfiguration.Object, reminderService.Object);
        }

        [Fact]
        public void Service_Init_With_NullArguments_Throws_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => new EventService(null, null, null, null,null,null,null,null));
        }

        [Fact]
        public void Service_Init_With_Valid_Arguments()
        {
            var service = new EventService(EventRepository,
                TenantTime.Object
                , EventHub.Object,
                mockLookUp.Object, emailService.Object, tempalteService.Object, eventConfiguration.Object, reminderService.Object);

            Assert.NotNull(service);
        }

        [Fact]
        public async void Service_Add_With_NULLOREMPTY_EntityId_EntityType_Throws_ArgumentException()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () => await Service.Add(null, "12345", new RequestPayload()));
            await Assert.ThrowsAsync<ArgumentException>(async () => await Service.Add("application", "", new RequestPayload()));
            await Assert.ThrowsAsync<ArgumentException>(async () => await Service.Add("application", "12345", new RequestPayload()));
        }

        [Fact]
        public void Service_Add_With_NULL_RequestPayload_Throws_ArgumentException()
        {
            Assert.ThrowsAsync<ArgumentException>(() => Service.Add("application", "12345",null));
        }

        [Fact]
        public void Service_Add_Valid_Arguments_Returns()
        {
            mockLookUp.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new Dictionary<string, string>
                {
                    { "loan","loan" },
                    { "merchant","merchant" },
                    { "application","application" },
                    { "borrower","borrower" },
                });

            EventHub.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()))
                .ReturnsAsync(true);

           Service.Add("application", "000001", new RequestPayload { EventDate =DateTime.Now,Description="test desctiption", Priority= EventPriority.High });


          
            EventHub.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.AtLeastOnce);

            
        }

        [Fact]
        public void Service_Add_InValid_EventDate_Throws_ArgumentException()
        {
            mockLookUp.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new Dictionary<string, string>
                {
                    { "loan","loan" },
                    { "merchant","merchant" },
                    { "application","application" },
                    { "borrower","borrower" },
                });

            EventHub.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()))
                .ReturnsAsync(true);

            Assert.ThrowsAsync<ArgumentException>(() => Service.Add("application", "000001", new RequestPayload { EventDate = null, Description = "test desctiption", Priority = EventPriority.High }));
            
        }


        [Fact]
        public async void Service_GetAll_Events()
        {
            var result = await Service.GetAll("application", "000001");

            Assert.NotNull(result);
        }


        [Fact]
        public async void Service_GetByMonth_Events()
        {

            EventRepository.Add(new EventModel { EntityType="application",EntityId="000001", EventDate = new DateTimeOffset(new DateTime(2017, 4, 5, 0, 0, 0))});
            EventRepository.Add(new EventModel { EntityType = "application", EntityId = "000001", EventDate = new DateTimeOffset(new DateTime(2017, 5, 5, 0, 0, 0)) });

            var result = await Service.GetByMonth("application", "000001",4,2017);

            Assert.NotNull(result);
            Assert.Equal(result.ToList().Count(), 1);
        }

        [Fact]
        public async void Service_GetByDate_Events()
        {
            EventRepository.Add(new EventModel { EntityType = "application", EntityId = "000001", EventDate = new DateTimeOffset(new DateTime(2017, 4, 5, 0, 0, 0)) });
            EventRepository.Add(new EventModel { EntityType = "application", EntityId = "000001", EventDate = new DateTimeOffset(DateTime.Now) });

            var result = await Service.GetByDate("application", "000001", DateTime.Now);

            Assert.NotNull(result);
          
        }

    }
}
