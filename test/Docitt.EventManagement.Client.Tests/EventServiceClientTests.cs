﻿using System.Collections.Generic;
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using Xunit;
using Docitt.EventManagement.Services;
using System;
using LendFoundry.Foundation.Client;

namespace Docitt.EventManagement.Client.Tests
{
    public class EventServiceClientTests
    {
        private Mock<IServiceClient> ServiceClient { get; }
        private IEventService Client { get; }
        private IRestRequest Request { get; set; }

        public EventServiceClientTests()
        {
            ServiceClient = new Mock<IServiceClient>();
            Client = new Docitt.EventManagement.Client.EventServiceClient(ServiceClient.Object);
        }

        [Fact]
        public void Client_Add()
        {
            ServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
             .Returns(true);

            Client.Add("application", "000001",new RequestPayload());

            ServiceClient.Verify(x => x.Execute(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }

        [Fact]
        public async void Client_GetAll()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<List<EventModel>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new List<EventModel>());

            var result = await Client.GetAll("application", "000001");

            ServiceClient.Verify(x => x.ExecuteAsync<List<EventModel>>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityid}/", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }


        [Fact]
        public async void Client_GetByMonth()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<List<EventModel>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new List<EventModel>());

            var result = await Client.GetByMonth("application", "000001", 4, 2017);

            ServiceClient.Verify(x => x.ExecuteAsync<List<EventModel>>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/{month}/{year}/month", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_GetByDate()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<List<EventModel>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .ReturnsAsync(new List<EventModel>());

            var result = await Client.GetByDate("application", "000001",new DateTimeOffset());

            ServiceClient.Verify(x => x.ExecuteAsync<List<EventModel>>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/date", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }


    }
}
