﻿using Docitt.EventManagement.Services;
using LendFoundry.Security.Tokens;

namespace Docitt.EventManagement.Client
{
    public interface IEventManagementClientFactory
    {
        IEventService Create(ITokenReader reader);
    }
}