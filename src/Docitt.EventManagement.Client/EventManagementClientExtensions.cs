﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;
namespace Docitt.EventManagement.Client
{
    public static class EventManagementClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddEventManagement(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IEventManagementClientFactory>(p => new EventManagementClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IEventManagementClientFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }

        public static IServiceCollection AddEventManagement(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IEventManagementClientFactory>(p => new EventManagementClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IEventManagementClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddEventManagement(this IServiceCollection services)
        {
            services.AddTransient<IEventManagementClientFactory>(p => new EventManagementClientFactory(p));
            services.AddTransient(p => p.GetService<IEventManagementClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        
    }
}
