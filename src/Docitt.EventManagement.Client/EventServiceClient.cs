﻿using Docitt.EventManagement.Services;
using LendFoundry.Foundation.Client;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.EventManagement.Client
{
    public class EventServiceClient : IEventService
    {
        public EventServiceClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task Add(string entityType, string entityId, IRequestPayload request)
        {
            await Client.PostAsync<IRequestPayload>($"/{entityType}/{entityId}/", request, true);
        }
        public async Task<bool> UpSertEvent(string entityType, string entityId, List<RequestPayload> eventList)
        {
            return await Client.PostAsync<List<RequestPayload>,dynamic>($"/{entityType}/{entityId}/modify", eventList, true);
        }

        public async Task<bool> RemoveEvent(string entityType, string entityId, string eventId)
        {
            var rest = new RestRequest("/{entityType}/{entityId}/{entityId}", Method.DELETE);
            rest.AddUrlSegment("entityType", entityType);
            rest.AddUrlSegment("entityId", entityId);
            rest.AddUrlSegment("entityId", entityId);
            return await Client.ExecuteAsync<bool>(rest);

           //return await Client.DeleteAsync($"/{entityType}/{entityId}/{entityId}");
        }

        public async Task<IEnumerable<IEventModel>> GetAll(string entityType)
        {
            return await Client.GetAsync<List<EventModel>>($"/{entityType}/");
        }

        public async Task<IEnumerable<IEventModel>> GetAll(string entityType, string entityId)
        {
             return await Client.GetAsync<List<EventModel>>($"/{entityType}/{entityId}/");
        }

        public async Task<IEnumerable<IEventModel>> GetByMonth(string entityType, string entityId, int month, int year)
        {
            return await Client.GetAsync<List<EventModel>>($"/{entityType}/{entityId}/{month}/{year}/month");
        }

        public async Task<IEnumerable<IEventModel>> GetByDate(string entityType, string entityId, DateTimeOffset date)
        {
            return await Client.GetAsync<List<EventModel>>($"/{entityType}/{entityId}/date/{date}");
        }

        public async Task<IEnumerable<IEventModel>> GetByDate(string entityType, DateTimeOffset date)
        {
               return await Client.GetAsync<List<EventModel>>($"/{entityType}/date/{date}");
        }

        public async Task<IEventModel> GetById(string entityType, string entityId, string eventId)
        {
             return await Client.GetAsync<EventModel>($"/{entityType}/{entityId}/{eventId}");
        }
        public async Task<bool> SendToCalendar(string entityType, string entityId, string email, IEnumerable<string> eventIds,string eventType)
        {
            // Event Type should be CALCEL or REQUEST
            var response = await Client.PostAsync<IEnumerable<string>, dynamic>($"/{entityType}/{entityId}/{email}/sendtocalendar/{eventType}", eventIds, true);
            return Convert.ToBoolean(response);

        }
    }
}
