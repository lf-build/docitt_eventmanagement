﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.EventManagement
{
    public interface IEventRepository : IRepository<IEventModel>
    {
        Task<IEnumerable<IEventModel>> GetAll(string entityType);
        Task<IEnumerable<IEventModel>> GetAll(string entityType, string entityId);

        Task<IEnumerable<IEventModel>> GetByMonth(string entityType, string entityId, DateTimeOffset firstDayofMonth, DateTimeOffset lastDayofMonth);

        Task<IEnumerable<IEventModel>> GetByDate(string entityType, string entityId, DateTimeOffset date);

        Task<IEnumerable<IEventModel>> GetByDate(string entityType, DateTimeOffset date);

        Task<IEventModel> GetById(string entityType, string entityId, string eventId);
        Task<IEnumerable<IEventModel>> GetByIds(string entityType, string entityId, IEnumerable<string> eventIds);

    }
}