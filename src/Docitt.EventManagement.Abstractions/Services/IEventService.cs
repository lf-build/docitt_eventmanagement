﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.EventManagement.Services
{
    public interface IEventService
    {
        Task Add(string entityType, string entityId, IRequestPayload eventData);

        Task<IEnumerable<IEventModel>> GetAll(string entityType);
        Task<IEnumerable<IEventModel>> GetAll(string entityType, string entityId);
        Task<IEnumerable<IEventModel>> GetByMonth(string entityType, string entityId, int month, int year);

        Task<IEnumerable<IEventModel>> GetByDate(string entityType, string entityId, DateTimeOffset date);

        Task<IEnumerable<IEventModel>> GetByDate(string entityType, DateTimeOffset date);

        Task<IEventModel> GetById(string entityType, string entityId, string eventId);
        Task<bool> SendToCalendar(string entityType, string entityId, string email, IEnumerable<string> eventIds, string eventType);

        Task<bool> RemoveEvent(string entityType, string entityId, string eventId);

        Task<bool> UpSertEvent(string entityType, string entityId, List<RequestPayload> eventList);
    }
}
