﻿
namespace Docitt.EventManagement.Events
{
    public class EventUpdated
    {
        public IEventModel Request { get; set; }
    }
}
