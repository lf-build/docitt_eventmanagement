﻿
namespace Docitt.EventManagement.Events
{
    public class EventRemoved
    {
        public IEventModel Request { get; set; }
    }
}
