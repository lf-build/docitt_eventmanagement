﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.EventManagement.Events
{
    public class EventRequestCreated
    {
        public IEventModel Request { get; set; }
    }
}
