﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.EventManagement.Configuration
{
    public class TemplateConfiguration :ITemplateConfiguration
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }
}
