﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.EventManagement.Configuration
{
    public class EventManagementConfiguration : IEventManagementConfiguration
    {
        public TemplateConfiguration VEventCaledarFormat { get; set; }
        public TemplateConfiguration EventEmailTemplate { get; set; }
        public string AddEventReminderDescription { get; set; }
        public string UpdatedEventReminderDescription { get; set; }
        public string RemoveEventReminderDescription { get; set; }
        public string Delimiter { get; set; }
        public string Database { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string ConnectionString { get; set; }
    }
}
