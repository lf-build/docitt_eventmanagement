﻿namespace Docitt.EventManagement.Configuration
{
    public interface ITemplateConfiguration
    {
        string Name { get; set; }
        string Version { get; set; }
    }
}