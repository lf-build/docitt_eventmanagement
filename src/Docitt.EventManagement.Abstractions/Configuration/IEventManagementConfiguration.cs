﻿using LendFoundry.Foundation.Client;

namespace Docitt.EventManagement.Configuration
{
    public interface IEventManagementConfiguration : IDependencyConfiguration
    {
        TemplateConfiguration EventEmailTemplate { get; set; }
        TemplateConfiguration VEventCaledarFormat { get; set; }
        string AddEventReminderDescription { get; set; }
        string UpdatedEventReminderDescription { get; set; }
        string RemoveEventReminderDescription { get; set; }
        string Delimiter { get; set; }
    }
}