﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;

namespace Docitt.EventManagement
{
    public interface IEventModel : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        string Title { get; set; }
        string Description { get; set; }
        string Location { get; set; }
        DateTimeOffset EventDate { get; set; }
        string StartTime { get; set; }
        string StartTimePeriod { get; set; }
        string EndTime { get; set; }
        string EndTimePeriod { get; set; }
        bool Is24HoursTimeFormat { get; set; }
        EventPriority Priority { get; set; }
        string Invities { get; set; }
        DateTimeOffset CreatedDate { get; set; }
        string CreatedBy { get; set; }
    }
}