﻿
namespace Docitt.EventManagement
{
    public enum EventPriority
    {
        Undefined,
        High,
        Low,
        Information,
        Medium
    }
}
