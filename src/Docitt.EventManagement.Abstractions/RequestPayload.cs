﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.EventManagement
{
    public class RequestPayload : IRequestPayload
    {
        public Nullable<DateTime> EventDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string StartTime { get; set; }
        public string StartTimePeriod { get; set; }
        public string EndTime { get; set; }
        public string EndTimePeriod { get; set; }
        public bool Is24HoursTimeFormat { get; set; }
        public string Invities { get; set; }
        public EventPriority Priority { get; set; }
        public string CreatedBy { get; set; }
        public string EventId { get; set; }
        public bool isAdded { get; set; }

    }
}
