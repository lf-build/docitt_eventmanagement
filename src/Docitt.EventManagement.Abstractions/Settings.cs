using  System;
namespace Docitt.EventManagement
{
    public static class Settings
    {
        
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "event-management";
    }
}