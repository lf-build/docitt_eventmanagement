﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.EventManagement
{
    public class EventModel : Aggregate, IEventModel
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }        
        public string Title { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public DateTimeOffset EventDate { get; set; }
        public string StartTime { get; set; }
        public string StartTimePeriod { get; set; }
        public string EndTime { get; set; }
        public string EndTimePeriod { get; set; }
        public bool Is24HoursTimeFormat { get; set; }
        public string Invities { get; set; }
        public EventPriority Priority { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        
    }
}
