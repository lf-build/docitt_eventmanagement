﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.EventManagement
{
    public class EventTemplateModel
    {
        public string eventDate { get; set; } //20170421T122432Z,
        public string eventStartTime { get; set; }  //20170421T122432Z,
        public string eventEndTime { get; set; }  //20170421T122432Z,
        public string eventLocation { get; set; }
        public string eventUID { get; set; }
        public string eventDescription { get; set; }
        public string eventTitle { get; set; }
        public string eventOrganizerEmail { get; set; }
        public string eventType { get; set; }
    }

    public enum EventType
    {
        REQUEST,
        CANCEL
    }
}
