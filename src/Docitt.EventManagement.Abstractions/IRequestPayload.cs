﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.EventManagement
{
    public interface IRequestPayload
    {
        Nullable<DateTime> EventDate { get; set; }
        string Title { get; set; }
        string Description { get; set; }
        string Location { get; set; }
        string StartTime { get; set; }
        string StartTimePeriod { get; set; }
        string EndTime { get; set; }
        string EndTimePeriod { get; set; }
        bool Is24HoursTimeFormat { get; set; }
        string Invities { get; set; }
        EventPriority Priority { get; set; }
        string CreatedBy { get; set; }
        string EventId { get; set; }
        bool isAdded { get; set; }
    }
}
