﻿
using Docitt.EventManagement.Services;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Docitt.EventManagement.Api.Controllers
{

    /// <summary>
    /// Represents api controller class.
    /// </summary>
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="service"></param>
        public ApiController(IEventService service)
        {
            if (service == null)
                throw new ArgumentException($"{nameof(service)} is mandatory");

            Service = service;
        }

        private IEventService Service { get; }
        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        /// <summary>
        /// Add event based on params entity type, entity id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("/{entityType}/{entityId}/")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Add(string entityType, string entityId, [FromBody]RequestPayload request)
        {
            try
            {
                await Service.Add(entityType, entityId, request);
                return NoContentResult;
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        /// <summary>
        /// Get all event data based on params entityType.
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        [HttpGet("/{entityType}")]
        [ProducesResponseType(typeof(IEnumerable<IEventModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetAll(string entityType)
        {
            return ExecuteAsync(async () => Ok(await Service.GetAll(entityType)));
        }

        /// <summary>
        /// Get all events based on entity type and entity id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{entityId}")]
        [ProducesResponseType(typeof(IEnumerable<IEventModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetAll(string entityType, string entityId)
        {
            return ExecuteAsync(async () => Ok(await Service.GetAll(entityType, entityId)));
        }

        /// <summary>
        /// Remove event based on entitytype,entityid and event id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="eventId"></param>
        /// <returns></returns>
        [HttpDelete("/{entityType}/{entityId}/{eventId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> RemoveEvent(string entityType, string entityId, string eventId)
        {
            return ExecuteAsync(async () => Ok(await Service.RemoveEvent(entityType, entityId, eventId)));
        }

        /// <summary>
        /// Add or update event based on entity type and entity id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="eventList"></param>
        /// <returns></returns>
        [HttpPost("/{entityType}/{entityId}/add-update-event")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpSertEvent(string entityType, string entityId, [FromBody]List<RequestPayload> eventList)
        {
            try
            {
                return Ok(await Service.UpSertEvent(entityType, entityId, eventList));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        /// <summary>
        /// Get event by entity type,entity id and event id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="eventId"></param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{entityId}/{eventId}")]
        [ProducesResponseType(typeof(IEventModel), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetById(string entityType, string entityId, string eventId)
        {
            return ExecuteAsync(async () => Ok(await Service.GetById(entityType, entityId, eventId)));
        }

        /// <summary>
        /// Send event to calendar.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="email"></param>
        /// <param name="eventIds"></param>
        /// <param name="eventType"></param>
        /// <returns></returns>
        // Event Type Should be CANCEL or REQUEST
        [HttpPost("/{entityType}/{entityId}/{email}/sendtocalendar/{eventType}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> SendToCalendar(string entityType, string entityId, string email, [FromBody]string[] eventIds, string eventType)
        {
            return ExecuteAsync(async () => Ok(await Service.SendToCalendar(entityType, entityId, email, eventIds, eventType)));
        }

        /// <summary>
        /// Get event by month
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{entityId}/{month}/{year}/month")]
        [ProducesResponseType(typeof(IEnumerable<IEventModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetByMonth(string entityType, string entityId, int month = 0, int year = 0)
        {
            return ExecuteAsync(async () => Ok(await Service.GetByMonth(entityType, entityId, month, year)));
        }

        /// <summary>
        /// Get event by date based on params: entity type, entity id and date.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{entityId}/date/{date}")]
        [ProducesResponseType(typeof(IEnumerable<IEventModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetByDate(string entityType, string entityId, string date)
        {
            if (string.IsNullOrEmpty(date))
                throw new ArgumentException($"{nameof(date)} is mandatory");

            //when calling from client that time we will get date as encoded string.so need to decode before parsing.
            date = WebUtility.UrlDecode(date);
            DateTimeOffset dateoffset;
            if (!DateTimeOffset.TryParse(date, out dateoffset))
            {
                throw new ArgumentException($"{nameof(date)} is not in proper format");
            }
            return ExecuteAsync(async () => Ok(await Service.GetByDate(entityType, entityId, dateoffset)));
        }

        /// <summary>
        /// Get event by date based on params: entity type and date
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [HttpGet("/{entityType}/date/{date}")]
        [ProducesResponseType(typeof(IEnumerable<IEventModel>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetByDate(string entityType, string date)
        {
            if (string.IsNullOrEmpty(date))
                throw new ArgumentException($"{nameof(date)} is mandatory");
            
            //when calling from client that time we will get date as encoded string.so need to decode before parsing.
            date = WebUtility.UrlDecode(date);
            
            DateTimeOffset dateoffset;
            if (!DateTimeOffset.TryParse(date, out dateoffset))
            {
                throw new ArgumentException($"{nameof(date)} is not in proper format");
            }

            return ExecuteAsync(async () => Ok(await Service.GetByDate(entityType, dateoffset)));
        }
    }
}
