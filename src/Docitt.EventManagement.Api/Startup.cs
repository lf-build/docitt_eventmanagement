﻿

using Docitt.EventManagement.Configuration;
using Docitt.EventManagement.Persistence;
using Docitt.EventManagement.Services;
using Docitt.ReminderService.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Email.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;

using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.TemplateManager.Client;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using System.Collections.Generic;
using LendFoundry.EventHub;
using LendFoundry.Configuration;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
#endif

namespace Docitt.EventManagement.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "DocittEventManagement"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
               {
                   { "Bearer", new string[] { } }
               });

                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Docitt.EventManagement.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#endif

            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantService();
            services.AddEventHub(Settings.ServiceName);
            services.AddConfigurationService<EventManagementConfiguration>(Settings.ServiceName);
        
            services.AddDependencyServiceUriResolver<EventManagementConfiguration>(Settings.ServiceName);

            services.AddEmailService();
            services.AddReminderService();
            services.AddLookupService();
            services.AddTemplateManagerService();
            //services.AddTransient<IEventHubClient>(p =>
            //{
            //    var currentTokenReader = p.GetService<ITokenReader>();
            //    var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
            //    return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            //});
            services.AddTransient<IEventManagementConfiguration>(provider => provider.GetRequiredService<IConfigurationService<EventManagementConfiguration>>().Get());

           
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddTransient<IEventRepository, EventRepository>();
            services.AddTransient<IEventService, EventService>();
            services.AddTransient<IEventModel, EventModel>();
            services.AddTransient<IRequestPayload, RequestPayload>();
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }
      
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		    app.UseCors(env);
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.

#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "DOCITT EventManagement Service");
            });
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();         
            app.UseMvc();
            app.UseConfigurationCacheDependency();
        }
    }
}