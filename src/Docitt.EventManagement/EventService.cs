﻿
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup;
using Docitt.EventManagement.Services;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using LendFoundry.Email;
using System.Text.RegularExpressions;
using LendFoundry.TemplateManager;
using Docitt.EventManagement.Configuration;
using Docitt.ReminderService.Services;
using LendFoundry.Foundation.Services;
using System.Globalization;
using LendFoundry.EventHub;

namespace Docitt.EventManagement
{
    public class EventService : IEventService
    {
        public EventService
        (
            IEventRepository repository,
            ITenantTime tenantTime,
            IEventHubClient eventhub,
            ILookupService lookup,
            IEmailService emailService,
          ITemplateManagerService templateManager,
          IEventManagementConfiguration configuration,
          IReminderService reminderService
        )
        {
            if (repository == null) throw new ArgumentException($"{nameof(repository)} is mandatory");
            if (tenantTime == null) throw new ArgumentException($"{nameof(tenantTime)} is mandatory");
            if (eventhub == null) throw new ArgumentException($"{nameof(eventhub)} is mandatory");
            if (lookup == null) throw new ArgumentException($"{nameof(lookup)} is mandatory");
            if (emailService == null) throw new ArgumentException($"{nameof(emailService)} is mandatory");
            Lookup = lookup;
            Repository = repository;
            TenantTime = tenantTime;
            EventHub = eventhub;
            _emailService = emailService;
            TemplateManager = templateManager;
            Configuration = configuration;
            Reminder_Service = reminderService;
        }

        private IReminderService Reminder_Service { get; }
        private IEventManagementConfiguration Configuration;
        private ITemplateManagerService TemplateManager { get; }
        private ILookupService Lookup { get; }
        private IEventHubClient EventHub { get; }

        IEmailService _emailService { get; }
        private ITenantTime TenantTime { get; }

        private IEventRepository Repository { get; }
        public async Task Add(string entityType, string entityId, IRequestPayload eventData)
        {
            // your code:        
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            entityType = EnsureEntityTypeIsValid(entityType);

            await AddEvent(entityType, entityId, eventData);
        }
        public async Task<IEnumerable<IEventModel>> GetAll(string entityType)
        {
            entityType = EnsureEntityTypeIsValid(entityType);

            return await Repository.GetAll(entityType);
        }


        public async Task<IEnumerable<IEventModel>> GetAll(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            EnsureEntityTypeIsValid(entityType);

            return await Repository.GetAll(entityType, entityId);

        }

        public async Task<IEnumerable<IEventModel>> GetByMonth(string entityType, string entityId, int month = 0, int year = 0)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            EnsureEntityTypeIsValid(entityType);

            if (month == 0)
                month = TenantTime.Now.Month;

            if (year == 0)
                year = TenantTime.Now.Year;

            var firstDayOfMonth = TenantTime.FromDate(new DateTime(year, month, 1, 0, 0, 0));

            var lastDayofMonth = TenantTime.FromDate(new DateTime(year, month, DateTime.DaysInMonth(year, month), 0, 0, 0));

            return await Repository.GetByMonth(entityType, entityId, firstDayOfMonth, lastDayofMonth);

        }

        public async Task<IEnumerable<IEventModel>> GetByDate(string entityType, string entityId, DateTimeOffset date)
        {
            EnsureEntityTypeIsValid(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            if (date == null)
                throw new ArgumentException($"{nameof(date)} is mandatory");

            var dateTimeOffset = TenantTime.FromDate(date.Date);
            return await Repository.GetByDate(entityType, entityId, dateTimeOffset);
        }

        public async Task<IEnumerable<IEventModel>> GetByDate(string entityType, DateTimeOffset date)
        {
            EnsureEntityTypeIsValid(entityType);

            if (date == null)
                throw new ArgumentException($"{nameof(date)} is mandatory");

            var dateTimeOffset = TenantTime.FromDate(date.Date);
            return await Repository.GetByDate(entityType, dateTimeOffset);
        }

        public async Task<bool> RemoveEvent(string entityType, string entityId, string eventId)
        {
            EnsureEntityTypeIsValid(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            if (string.IsNullOrWhiteSpace(eventId))
                throw new ArgumentException($"{nameof(eventId)} is mandatory");

            var eventData = await Repository.GetById(entityType, entityId, eventId);

            if (eventData == null)
                throw new ArgumentException($"Event : {eventId} not found");

            var monthinfo = String.Format("{0:MMMM}", eventData.EventDate) + " " + eventData.EventDate.Date.Day;
            var description = string.Format(Configuration.RemoveEventReminderDescription, monthinfo);

            AddNotification(entityType, entityId, description, eventData.Invities);

            //send email to all the invities
            SendEventInvite(entityType, entityId, eventData.Invities, eventData.Id, EventType.CANCEL.ToString());

            Repository.Remove(eventData);

            await EventHub.Publish($"{UppercaseFirst(entityType)}{nameof(Events.EventRemoved)}", new Events.EventRemoved { Request = eventData });

            return true;
        }

        private void AddNotification(string entityType, string entityId, string description, string invities)
        {
            string[] invitiesEmail = invities.Split(Configuration.Delimiter[0]);
            if (invitiesEmail.Length > 0)
            {
                invitiesEmail = invitiesEmail.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();

                foreach (var invitieEmail in invitiesEmail)
                {
                    ReminderService.IRequestPayload objPayload = new ReminderService.RequestPayload();
                    objPayload.Description = description;

                    if (!string.IsNullOrWhiteSpace(invitieEmail))
                        Reminder_Service.Add(entityType, entityId, invitieEmail, objPayload);
                    else
                        Reminder_Service.Add(entityType, entityId, string.Empty, objPayload);
                }
            }
        }

        public async Task<bool> UpSertEvent(string entityType, string entityId, List<RequestPayload> eventList)
        {
            EnsureEntityTypeIsValid(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            if (eventList == null)
                throw new ArgumentException("Payload cannot be empty");

            if (eventList.Count == 0)
                throw new ArgumentException("Payload cannot be empty");

            foreach (var eventData in eventList)
            {
                if (eventData.isAdded)
                {
                    await Task.Run(() => AddEvent(entityType, entityId, eventData));
                }
                else
                {
                    await Task.Run(() => UpdateEvent(entityType, entityId, eventData));
                }
            }

            return true;
        }

        public async Task<IEventModel> GetById(string entityType, string entityId, string eventId)
        {
            EnsureEntityTypeIsValid(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            if (string.IsNullOrWhiteSpace(eventId))
                throw new ArgumentException($"{nameof(eventId)} is mandatory");

            return await Repository.GetById(entityType, entityId, eventId);

        }

        public async Task<bool> SendToCalendar(string entityType, string entityId, string email, IEnumerable<string> eventIds, string eventType)
        {
            EnsureEntityTypeIsValid(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentException($"{nameof(email)} is mandatory");

            if (!IsValidEmail(email))
                throw new ArgumentException("Email property doesn't have valid value");

            if (eventIds.Count() == 0)
                throw new ArgumentException($"{nameof(eventIds)} is mandatory");

            var eventList = await Repository.GetByIds(entityType, entityId, eventIds);
            List<EmailAttachmentDetails> objAttachment = new List<EmailAttachmentDetails>();
            if (eventList.Count() > 0)
            {
                foreach (var eventDetail in eventList)
                {
                    var objData = MapToEventTemplate(eventDetail, eventType);
                    var templateResult = await TemplateManager.ProcessActiveTemplate(Configuration.VEventCaledarFormat.Name, Format.Text, objData);

                    if (templateResult != null)
                    {
                        string str = templateResult.Data;
                        str = str.Replace("##", "\r\n");
                        MemoryStream stringInMemoryStream = new MemoryStream(ASCIIEncoding.Default.GetBytes(str));
                        objAttachment.Add(new EmailAttachmentDetails() { FileName = "Meeting_" + objData.eventDate + ".ics", Content = stringInMemoryStream });
                    }
                    else
                    {
                        throw new ArgumentNullException($"Either {Configuration.VEventCaledarFormat.Name} is not found or error in processing it");
                    }
                }

                var data = new { name = "", Email = email };
                
                await _emailService.Send(entityType, entityId, Configuration.EventEmailTemplate.Name, data, objAttachment);

                return true;
            }
            else
            {
                throw new ArgumentException($"events information not found");
            }

        }

        private async Task AddEvent(string entityType, string entityId, IRequestPayload eventData)
        {
            EnsureDataIsValid(eventData);

            var objEvent = new EventModel
            {
                EventDate = TenantTime.FromDate(eventData.EventDate.GetValueOrDefault().Date),
                EntityType = entityType,
                EntityId = entityId,
                Title = eventData.Title,
                Description = eventData.Description,
                Location = eventData.Location,
                StartTime = eventData.StartTime.Trim(),
                StartTimePeriod = eventData.Is24HoursTimeFormat ? string.Empty : eventData.StartTimePeriod,
                EndTime = string.IsNullOrWhiteSpace(eventData.EndTime) ? "" : eventData.EndTime.Trim(),
                EndTimePeriod = eventData.Is24HoursTimeFormat ? string.Empty : eventData.EndTimePeriod,
                Is24HoursTimeFormat = eventData.Is24HoursTimeFormat,
                Invities = eventData.Invities,
                Priority = eventData.Priority,
                CreatedBy = eventData.CreatedBy,
                CreatedDate = TenantTime.Now
            };

            Repository.Add(objEvent);

            var monthinfo = String.Format("{0:MMMM}", objEvent.EventDate) + " " + objEvent.EventDate.Date.Day;
            var description = string.Format(Configuration.AddEventReminderDescription, monthinfo);
            AddNotification(entityType, entityId, description, eventData.Invities);

            //send email to all the invities
            SendEventInvite(entityType, entityId, eventData.Invities, objEvent.Id, EventType.REQUEST.ToString());

            await EventHub.Publish($"{UppercaseFirst(entityType)}{nameof(Events.EventRequestCreated)}", new Events.EventRequestCreated { Request = objEvent });
        }

        private void SendEventInvite(string entityType, string entityId, string invities, string eventId, string eventType)
        {
            string[] invitiesEmail = invities.Split(Configuration.Delimiter[0]);

            if (invitiesEmail.Length > 0)
            {
                invitiesEmail = invitiesEmail.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();

                List<string> eventids = new List<string>();
                eventids.Add(eventId);
                foreach (var invitieEmail in invitiesEmail)
                {
                    var flag = SendToCalendar(entityType, entityId, invitieEmail, eventids, eventType).Result;
                }
            }
        }

        private async Task UpdateEvent(string entityType, string entityId, IRequestPayload eventData)
        {
            EnsureDataIsValid(eventData);

            var objData = Repository.GetById(entityType, entityId, eventData.EventId).Result;
            if (objData != null)
            {
                objData.Invities = eventData.Invities;
                objData.Priority = eventData.Priority;
                objData.Title = eventData.Title;
                objData.Description = eventData.Description;
                objData.Location = eventData.Location;
                objData.StartTime = eventData.StartTime.Trim();
                objData.StartTimePeriod = eventData.Is24HoursTimeFormat ? string.Empty : eventData.StartTimePeriod;
                objData.EndTime = eventData.EndTime.Trim();
                objData.EndTimePeriod = eventData.Is24HoursTimeFormat ? string.Empty : eventData.EndTimePeriod;
                objData.Is24HoursTimeFormat = eventData.Is24HoursTimeFormat;
                Repository.Update(objData);

                var monthinfo = String.Format("{0:MMMM}", objData.EventDate) + " " + objData.EventDate.Date.Day;
                var description = string.Format(Configuration.UpdatedEventReminderDescription, monthinfo);
                AddNotification(entityType, entityId, description, eventData.Invities);

                //send email to all the invities                
                SendEventInvite(entityType, entityId, eventData.Invities, objData.Id, EventType.REQUEST.ToString());

                await EventHub.Publish($"{UppercaseFirst(entityType)}{nameof(Events.EventUpdated)}", new Events.EventUpdated { Request = objData });
            }
        }

        private EventTemplateModel MapToEventTemplate(IEventModel objEvent, string eventType)
        {
            try
            {
                DateTimeOffset eventDate = objEvent.EventDate;
                DateTime eventStartDate = eventDate.DateTime,
                         eventEndDate = eventDate.DateTime;

                DateTime dtStart = eventDate.DateTime,
                         dtEnd = eventDate.DateTime;

                if (objEvent.Is24HoursTimeFormat)
                {
                    DateTime.TryParse(objEvent.StartTime, out dtStart);
                    DateTime.TryParse(objEvent.EndTime, out dtEnd);
                }
                else
                {
                    DateTime.TryParse(objEvent.StartTime + objEvent.StartTimePeriod, out dtStart);
                    DateTime.TryParse(objEvent.EndTime + objEvent.EndTimePeriod, out dtEnd);
                }

                eventStartDate = new DateTime(eventDate.Year, eventDate.Month, eventDate.Day, dtStart.Hour, dtStart.Minute, 0);
                eventEndDate = new DateTime(eventDate.Year, eventDate.Month, eventDate.Day, dtEnd.Hour, dtEnd.Minute, 0);

                var eventTemplate = new EventTemplateModel()
                {
                    eventStartTime = string.Format("{0:yyyyMMddTHHmmssZ}", eventStartDate),
                    eventEndTime = string.Format("{0:yyyyMMddTHHmmssZ}", eventEndDate),
                    eventDate = string.Format("{0:yyyyMMddTHHmmssZ}", objEvent.EventDate),
                    eventLocation = objEvent.Location,
                    eventUID = string.Format("{0}", Guid.NewGuid()),
                    eventDescription = objEvent.Description,
                    eventTitle = objEvent.Description,
                    eventOrganizerEmail = objEvent.CreatedBy
                };


                if (!string.IsNullOrWhiteSpace(eventType) && Enum.IsDefined(typeof(EventType), eventType.Trim().ToUpper()))
                    eventTemplate.eventType = eventType.Trim().ToUpper();
                else
                    eventTemplate.eventType = EventType.REQUEST.ToString();

                return eventTemplate;
            }
            catch (Exception ex)
            {
                throw new InvalidArgumentException(ex.Message);
            }
        }

        private bool EnsureInvitieListIsValid(string invitiesList)
        {
            string[] invitiesEmail = invitiesList.Split(Configuration.Delimiter[0]);

            if (invitiesEmail.Length > 0)
            {
                invitiesEmail = invitiesEmail.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();

                foreach (var invitieEmail in invitiesEmail)
                {
                    if (!IsValidEmail(invitieEmail))
                        return false;
                }
            }

            return true;
        }

        private static bool IsValidEmail(string email)
        {
            return Regex.IsMatch(email,
                @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z",
                RegexOptions.IgnoreCase | RegexOptions.Compiled);
        }

        private void EnsureDataIsValid(IRequestPayload payload)
        {
            if (payload == null)
                throw new ArgumentException($"{nameof(payload)} is mandatory");

            if (payload.EventDate == null)
                throw new ArgumentException($"{nameof(payload.EventDate)} is mandatory");

            if (payload.Priority == EventPriority.Undefined)
                throw new ArgumentException($"{nameof(payload.Priority)} is mandatory");

            if (payload.EventDate < TenantTime.Today.Date)
                throw new ArgumentException("we can't create an event for the past date.");

            if (!EnsureInvitieListIsValid(payload.Invities))
                throw new ArgumentException("Invities email is not proper format.if multiple invities then seperate them by commas.");

            ValidateEventTime(payload);
        }

        private void ValidateEventTime(IRequestPayload eventData)
        {
            string startTimeToValidate = eventData.StartTime != null ? eventData.StartTime.Trim() : string.Empty;
            string endTimeToValidate = eventData.EndTime != null ? eventData.EndTime.Trim() : string.Empty;

            Regex regEx;

            if (eventData.Is24HoursTimeFormat)
            {
                regEx = new Regex(@"^(?:[01][0-9]|2[0-3]):[0-5][0-9]$");
            }
            else
            {
                regEx = new Regex(@"^(?:0?[0-9]|1[0-2]):[0-5][0-9] [ap]m$", RegexOptions.IgnoreCase);
                startTimeToValidate = startTimeToValidate + " " + eventData.StartTimePeriod;
                endTimeToValidate = endTimeToValidate + " " + eventData.EndTimePeriod;
            }

            if (!regEx.IsMatch(startTimeToValidate))
                throw new ArgumentException($"{nameof(eventData.StartTime)} is invalid");

            if (!string.IsNullOrWhiteSpace(eventData.EndTime) && !regEx.IsMatch(endTimeToValidate))
                throw new ArgumentException($"{nameof(eventData.EndTime)} is invalid");
        }

        private string EnsureEntityTypeIsValid(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();

            if (Lookup.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity");
            return entityType;
        }

        private static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return $"{char.ToUpper(s[0])}{s.Substring(1)}";
        }
    }
}
