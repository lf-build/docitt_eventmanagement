﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using Docitt.EventManagement;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Globalization;
using System;
using System.Linq;

namespace Docitt.EventManagement.Persistence
{
    public class EventRepository : MongoRepository<IEventModel,EventModel>,IEventRepository
    {        
        static EventRepository()
        {
            BsonClassMap.RegisterClassMap<EventModel>(map =>
            {
                map.AutoMap();
                map.MapMember(f => f.Priority).SetSerializer(new EnumSerializer<EventPriority>(BsonType.String));
                map.MapMember(m => m.EventDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.CreatedDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                var type = typeof(EventModel);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public EventRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "eventmanagement")
        {
            CreateIndexIfNotExists("entity-type", Builders<IEventModel>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType));
            CreateIndexIfNotExists("entity-type-id", Builders<IEventModel>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId));
        }


        public async Task<IEnumerable<IEventModel>> GetAll(string entityType)
        {
            return await Query.Where(p => p.EntityType == entityType).ToListAsync();
        }

        public async Task<IEnumerable<IEventModel>> GetAll(string entityType, string entityId)
        {
            return await Query.Where(p => p.EntityType == entityType && p.EntityId == entityId).ToListAsync();
        }

        public async Task<IEnumerable<IEventModel>> GetByMonth(string entityType, string entityId,  DateTimeOffset firstDayofMonth, DateTimeOffset lastDayofMonth)
        {
            return await Query.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.EventDate >= firstDayofMonth && p.EventDate <= lastDayofMonth).ToListAsync();
        }

        public async Task<IEnumerable<IEventModel>> GetByDate(string entityType, string entityId, DateTimeOffset date)
        {
            //var filter = Builders<IEventModel>.Filter.Where(x => x.EntityType == entityType && x.EntityId == entityId && x.EventDate == date);
            ////var update = Builders<IReminderModel>.Update.Set(x => x.isRead, true);
            //return await Collection.FindAsync(filter).Result.ToListAsync();

            return await Query.Where(p => p.EntityType == entityType && p.EntityId == entityId && p.EventDate == date ).ToListAsync();
        }

        public async Task<IEnumerable<IEventModel>> GetByDate(string entityType, DateTimeOffset date)
        {
            return await Query.Where(p => p.EntityType == entityType && p.EventDate == date).ToListAsync();
        }

        public async Task<IEventModel> GetById(string entityType, string entityId,string eventId)
        {
           return await Query.FirstOrDefaultAsync<IEventModel>(p => p.EntityType == entityType && p.EntityId == entityId && p.Id == eventId);
        }

        public async Task<IEnumerable<IEventModel>> GetByIds(string entityType, string entityId, IEnumerable<string> eventIds)
        {  
            return await Query.Where(p => p.EntityType == entityType && p.EntityId == entityId && eventIds.Contains(p.Id)).ToListAsync();
        }
    }
}
