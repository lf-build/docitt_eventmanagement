
# Service Name : docitt_eventmanagement

- The purpose of eventmanagement service is to add, update, delete and get events.

## Getting Started

- Require environment setup.
- Repository and MongoDB access.
- Should have knowledge of Bit-bucket and DotNet CLI(Command-line interface ) commands. 
- Helpful links to start work :
	* https://docs.microsoft.com/en-us/dotnet/core/
	* https://docs.microsoft.com/en-us/dotnet/core/tools/?tabs=netcore2x
		
### Prerequisites

 - Windows, macOS or Linux Operating System.
 - .NET Core SDK (Software Development Kit).
 - .NET Core Command-line interface (CLI) or Visual Studio 2017.
 - MongoDB Management Tool.
 
### Installing

 - Clone source using below git command or source tree

	    git clone <repository_path>
	And go to respective branch for ex develop

		git fetch && git checkout develop	

 - Open command prompt and goto project directory.

 - Restore nuget packages by command

 		dotnet restore -s <Nuget Package Source> <solution_file>

		Ex. dotnet restore -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc Docitt.EventManagement.2017.sln 

 - Build solution by command : 

		dotnet build --no-restore <solution_file>

		Ex. dotnet build --no-restore Docitt.EventManagement.2017.sln
		
 - Run Solution by command
 
		dotnet run --no-build --project <project path>|
		eg. dotnet run --no-build --project src\Docitt.EventManagement.Api\Docitt.EventManagement.Api.csproj
		
## Running the tests

- We have created shellscript file(cover.sh) for running tests and code coverage, which are shared on lendfoundry documents.
- To start the test
	* open cmd 
	* goto project directory
	* type command cover.sh and enter
- you can see all tests results and code coverate report with covered number of lines and percentage on cmd.
- Once testing completed successfully from above command code coverage report html file is geneated on path :
 artifacts\coverage\report\coverage_report\index.html 

### What these tests test and why

- We are using xUnit testing and implementing for below projects.
	* Api
	* Client
	* Service
- xUnit.net is a free, open source, community-focused unit testing tool for the .NET Framework.
- This test includes all the scenarios which can be happen on realistic usage of this service.
- Help URL : https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test

## Service Configuration

- Name :  event-management .
- Can get configuration by request below URL and token for specific tenant from postman
	* URL : <configuration_service>/event-management
	* Sample configurations :
	```
{
    "vEventCaledarFormat": {
        "name": "VCalenderFormat",
        "version": "1.1"
    },
    "eventEmailTemplate": {
        "name": "vcalendaremail",
        "version": "1.0"
    },
    "addEventReminderDescription": "you have a new event coming up on {0}",
    "updatedEventReminderDescription": "you have a updated event coming up on {0}",
    "removeEventReminderDescription": "event removed on {0}",
    "delimiter": ",",
    "database": "eventmanagement"
}
	```


## Service Rules

NA

## Service API Documentation

- Anyone can access Api end points by entering below URL in any browser whenever service is running locally

		http://localhost:5000/swagger/

## Databases owned by email service

- Database : eventmanagement
- Collections : eventmanagement


## Environment Variables used by email service

- Envirounment variables are application variables which are used for setting application behaviour at runtime. Ex. We can use different envirounment variables for different servers like DEV, QA etc.
- In lendfoundry any single service is dependent or uses many other lendfoundry services, So for dynamically target different services we are setting envirounment variable in launchSettings.json.
- Below are the envirounment variables in which we are setting only
important services URL's which are used in mostly all lendfoundry services and we are setting it in "src\Docitt.Application.Api\Properties\launchSettings.json".
- Other dependent service URL's are taken from configurations.

		"CONFIGURATION_NAME": "event-management",
		"CONFIGURATION_URL": <configuration service URL>,
		 // ex. "CONFIGURATION_URL": "http://foundation.dev.lendfoundry.com:7001",
		"EVENTHUB_URL": <event hub URL>,
		"NATS_URL": <nats server URL>,
		"TENANT_URL": <tenant server URL>,
		"LOG_LEVEL": "Debug"

## Known Issues and Workarounds

